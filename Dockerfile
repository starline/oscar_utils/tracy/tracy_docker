
FROM ubuntu:jammy as base-stage

RUN apt update && \
    apt install -y \
    libglfw3 \
    libfreetype6 \
    libcapstone4 \
    libdbus-1-3 && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

FROM base-stage as build-common-stage
ARG TRACY_VERSION=v0.10

RUN apt update && \
    apt install -y \
    make \
    git \
    sudo \
    clang \
    libwayland-dev \
    libfreetype6-dev \
    libcapstone-dev \
    pkg-config \
    libxkbcommon-dev \
    libdbus-1-dev  \
    libglfw3-dev && \
    apt clean && \
    rm -rf /var/lib/apt/lists/*

ENV CXX=clang++
ENV CC=clang

RUN git clone -b v$TRACY_VERSION https://gitlab.com/starline/oscar_utils/tracy/tracy.git

FROM build-common-stage as build-profiler-stage
ARG N_PROC=4

RUN cd tracy/profiler/build/unix && \
    make release -j$N_PROC LEGACY=1

FROM build-common-stage as build-capture-stage
ARG N_PROC=4

RUN cd tracy/capture/build/unix && \
    make release -j$N_PROC LEGACY=1

FROM base-stage as runtime-profiler-stage

COPY --from=build-profiler-stage /tracy/profiler/build/unix/Tracy-release /workspace/executable
COPY ./env_script.bash  /workspace/env_script.bash

WORKDIR /workspace

ENTRYPOINT ["/workspace/env_script.bash", "/workspace/executable"]

FROM base-stage as runtime-capture-stage

COPY --from=build-capture-stage /tracy/capture/build/unix/*-release /workspace/executable
COPY ./env_script.bash  /workspace/env_script.bash

WORKDIR /workspace/save_dir

ENTRYPOINT ["/workspace/env_script.bash", "/workspace/executable"]
