#!/usr/bin/env bash

TRACY_VERSION=0.10
IMAGE_VERSION="0.0.1"

targets=(profiler capture)

for target in ${targets[@]}; do
        docker build \
                -t tracy_${target}_v$TRACY_VERSION:$IMAGE_VERSION \
                -t tracy_${target}_v$TRACY_VERSION:latest \
                --target runtime-$target-stage \
                --build-arg N_PROC=$(nproc) \
                --build-arg TRACY_VERSION=$TRACY_VERSION \
                --network host \
                .
done
