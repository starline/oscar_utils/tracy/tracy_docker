# Tracy Docker

[Русская версия Readme](README_RU.md)

Repository that builds Docker images with Tracy GUI profiler and Capture application.

[Official repository of Tracy](https://github.com/wolfpld/tracy)

Was tested on Ubuntu 20.04.

## Run:

```
./capture.bash
./profiler.bash
```

## Build 

If you want to modify something, script ```build.bash``` is used to rebuild images locally.

