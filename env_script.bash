#!/usr/bin/env bash
set -e

cleanup() {
    echo
    exit
}

trap cleanup INT TERM

"$@"
